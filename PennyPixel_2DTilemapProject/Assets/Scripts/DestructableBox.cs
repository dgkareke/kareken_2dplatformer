﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableBox : MonoBehaviour
{
    GameObject player;
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnMouseDown()
    {
        Destroy(gameObject);
    }
}
