﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        //destroy enemy
        if (other.gameObject.CompareTag("Enemy"))
        {
            Destroy(other.gameObject);
        }

        //ignore background collider and destroy projectile on anything else
        else if (other.gameObject.CompareTag("StageBounds") || other.gameObject.CompareTag("Player"))
        {
            return;
        }

        else
        {
            Destroy(gameObject);
        }
        
    }
}
