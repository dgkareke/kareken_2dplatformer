﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialText : MonoBehaviour
{
    public GameObject[] texts;
    public GameObject panel;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Time.timeScale = 1f;
            panel.SetActive(false);
        }
    }

    public void tutorialOn (int text)
    {
        panel.SetActive(true);

        Time.timeScale = 0f;

        for (int i = 0; i < texts.Length; i++)
        {
            if (i == text)
            {
                texts[i].SetActive(true);
            }

            else
            {
                texts[i].SetActive(false);
            }
        }
    }
}
