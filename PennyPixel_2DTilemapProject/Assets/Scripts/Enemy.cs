﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    GameObject player;
    private SpriteRenderer spriteRenderer;
    Rigidbody2D rb; //refernce to self rigidbody
    public float speed;


    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Set up references:
        //search for gameObject tagged "Player"
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody2D>(); //save reference to rigidbody
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Follow player
        Vector2 dirToPlayer = player.transform.position - transform.position; //get direction between this enemy and player

        if (dirToPlayer.x < 0)
        {
            spriteRenderer.flipX = true;
        }
        else
        {
            spriteRenderer.flipX = false;
        }

        dirToPlayer = dirToPlayer.normalized;
        rb.AddForce(dirToPlayer * speed); //moves enemy
    }
}
