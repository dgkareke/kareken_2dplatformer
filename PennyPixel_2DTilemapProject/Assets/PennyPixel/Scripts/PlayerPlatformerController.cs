﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPlatformerController : PhysicsObject {

    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    public float shotSpeed = 10;
    public float ammo = 0f;

    public bool hasWeapon = false;

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    public GameObject projectilePrefab;
    public Transform shotSpawn;

    // Use this for initialization
    void Awake () 
    {
        spriteRenderer = GetComponent<SpriteRenderer> (); 
        animator = GetComponent<Animator> ();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S) && hasWeapon)
        {
            GameObject projectile = Instantiate(projectilePrefab, shotSpawn.transform.position, projectilePrefab.transform.rotation);
            Rigidbody2D projectileRB = projectile.GetComponent<Rigidbody2D>();
            float directedSpeed = shotSpeed;

            //check which direction player is facing to shoot forward
            if (spriteRenderer.flipX == true)
            {
                 directedSpeed = -directedSpeed;
            }

            projectileRB.velocity = transform.right * directedSpeed;
            ammo--;

            if (ammo < 1)
            {
                hasWeapon = false;
            }
        }

        VelocityRefresh();
    }

    protected override void VelocityRefresh()
    {
        base.VelocityRefresh();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        //touching weapon pickup allows player to shoot projectiles
        if (other.gameObject.CompareTag("Weapon"))
        {
            hasWeapon = true;
            ammo = 10f;
            Destroy(other.gameObject);
        }

        //touching an enemy results in a game over
        else if(other.gameObject.CompareTag("Enemy")) {
            gameObject.SetActive(false);
            Invoke("RestartLevel", 2f);
        }
    }

    //specifically for falling out of stage to restart level
    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("StageBounds"))
        {
            gameObject.SetActive(false);
            Invoke("RestartLevel", 2f);
        }
    }
        
    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis ("Horizontal");

        if (Input.GetButtonDown ("Jump") && grounded) {
            velocity.y = jumpTakeOffSpeed;
        } else if (Input.GetButtonUp ("Jump")) 
        {
            if (velocity.y > 0) {
                velocity.y = velocity.y * 0.5f;
            }
        }

        if(move.x > 0.01f)
        {
            if(spriteRenderer.flipX == true)
            {
                spriteRenderer.flipX = false;
            }
        } 
        else if (move.x < -0.01f)
        {
            if(spriteRenderer.flipX == false)
            {
                spriteRenderer.flipX = true;
            }
        }

        animator.SetBool ("grounded", grounded);
        animator.SetFloat ("velocityX", Mathf.Abs (velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }

    //reloads level after player dies or all collectables are picked up
    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}